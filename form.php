<?php

$name = $_POST['name'] ?? '';
$email = $_POST['email'] ?? '';
$message = $_POST['message'] ?? '';

if ($name && $email && $message) {
    try {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception('Incorrect email');
        }

        try {
            $conn = new PDO("mysql:host=host;dbname=dbname", 'user', 'password', [
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
            ]);
        } catch (PDOException $e) {
            throw new Exception("Connection error, sorry");
        }

        $query = $conn->prepare("INSERT INTO table (name, email, message) VALUES (:name, :email, :message)");
        $query->bindParam(':name', $name);
        $query->bindParam(':email', $email);
        $query->bindParam(':message', $message);
        $query->execute();
    } catch (Exception $e) {
        $errorMessage = $e->getMessage();
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<?=$errorMessage ?? ''?>
<form method="post">
    <label for="name">Name</label><br>
    <input id="name" type="text" name="name" required value="<?=$name?>"><br>
    <label for="email">Email</label><br>
    <input id="email" type="email" name="email" required value="<?=$email?>"><br>
    <label for="message">Message</label><br>
    <textarea id="message" name="message" rows="10" cols="30" required><?=$message?></textarea><br>
    <input type="submit" value="submit">
</form>
</body>
</html>